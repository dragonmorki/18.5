﻿#include <iostream>
#include <algorithm>

class Player {
public:
    std::string name;
    int score;
};

bool comparePlayers(const Player& p1, const Player& p2) {
    return p1.score > p2.score;
}

int main() {
    int numPlayers;
    std::cout << "Введите количество игроков: ";
    std::cin >> numPlayers;

    Player* players = new Player[numPlayers];

    for (int i = 0; i < numPlayers; i++) {
        std::cout << "Введите имя игрока " << i + 1 << ": ";
        std::cin >> players[i].name;
        std::cout << "Введите количество очков, набранных игроком " << players[i].name << ": ";
        std::cin >> players[i].score;
    }

    std::sort(players, players + numPlayers, comparePlayers);

    std::cout << "\nИмена и очки игроков в отсортированном виде:\n";
    for (int i = 0; i < numPlayers; i++) {
        std::cout << i + 1 << ". " << players[i].name << " - " << players[i].score << " очков\n";
    }

    delete[] players;

    return 0;
}

